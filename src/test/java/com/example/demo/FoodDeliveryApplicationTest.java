package com.example.demo;

import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.isNotNull;

import java.util.List;
import java.util.Random;

import com.example.demo.entity.OrderEntity;
import com.example.demo.entity.PayMentEntity;
import com.example.demo.entity.RestaurantEntity;
import com.example.demo.repository.MenuRepo;
import com.example.demo.repository.OrderRepo;
import com.example.demo.repository.PaymentRepo;
import com.example.demo.repository.RestaurantRepo;
import com.example.demo.service.MenuService;
import com.example.demo.service.OrderService;
import com.example.demo.service.PaymentService;
import com.example.demo.service.RestaurantService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FoodDeliveryApplicationTest {

    @Autowired
    RestaurantRepo restRepo;

    @Autowired
    MenuRepo menuRepo;

    @Autowired
    OrderRepo orderRepo;

    @Autowired
    PaymentRepo paymentRepo;

    @Autowired
    RestaurantService restService;

    @Autowired
    MenuService menuService;

    @Autowired
    OrderService OrderService;

    @Autowired
    PaymentService paymentService;

    @Test
    public void getAllRestaurants() {
        List<RestaurantEntity> list = restService.getAllRestaurants();
        assertThat(list, isNotNull());
    }

    @Test
    public void getRestaurant(long id) {
        assertThat( restService.getRestuarantById(id), isNotNull());
    }

    // Menu service tests
    @Test
    public void getMenu(long id){
        assertThat( menuService.getItemList(id), isNotNull());
    }

    //Order servie tests

    @Test
    public void placeOrder(){
        assertThat(OrderService.placeOrder(getOrderEntity()), isNotNull());
    }

    @Test
    public void getOrderDetails(long id){
        assertThat(OrderService.getOrderDetails(id), isNotNull());
    }

    @Test
    public void deleteOrder(long id){
        OrderService.deleteOrder(id);
    }

    public OrderEntity getOrderEntity(){
        final OrderEntity orderEntity = new OrderEntity();
        final Random randomno = new Random();
        orderEntity.setOrderId(randomno.nextLong());
        orderEntity.setOrderAmount(450);
        orderEntity.setOrderStatus(2);
        orderEntity.setUserId(12452);
        return orderEntity;
    }

    //Payment service tests
    @Test
    public void insertPayment(){
        paymentService.insertPayment(getPaymentEntity());
    }

    public PayMentEntity getPaymentEntity(){
        PayMentEntity payMentEntity = new PayMentEntity();
        Random randomno = new Random();
        payMentEntity.setAmount(250);
        payMentEntity.setPaymentId(randomno.nextLong());
        payMentEntity.setOrderId(randomno.nextLong());
        payMentEntity.setPaymentStatus(1);
        return payMentEntity;
    }

}