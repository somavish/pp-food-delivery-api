package com.example.demo.controller;

import com.example.demo.entity.OrderEntity;
import com.example.demo.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
public class OrderController {
    
    @Autowired
    private OrderService orderService;

    @GetMapping(value = "/details/{orderId}")
    public OrderEntity getOrderDetails(@PathVariable("orderId") long id){
        return orderService.getOrderDetails(id);
    }

    @PostMapping(value = "/new")
    public void createOrder(@RequestBody  OrderEntity entity){
        orderService.placeOrder(entity);
    }

    @DeleteMapping(value = "/remove/{orderId}")
    public void deleteOrder(@PathVariable("orderId") long orderId){
        orderService.deleteOrder(orderId);
    }

    @PutMapping(value = "/update/")
    public void updateOrder(@RequestBody OrderEntity entity){
        orderService.updateOrder(entity);
    }

}