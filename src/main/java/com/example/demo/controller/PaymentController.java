package com.example.demo.controller;

import com.example.demo.entity.PayMentEntity;
import com.example.demo.service.impl.PaymentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order/payment")
public class PaymentController {

    @Autowired
    private PaymentServiceImpl paymentService;
    
    @PostMapping(value = "/create")
    public void initiatePayment(@RequestBody PayMentEntity entity){
        paymentService.insertPayment(entity);
    }

    @DeleteMapping(value = "/remove/{paymentId}")
    public void deletePayment(@PathVariable("paymentId")long paymentId){
        paymentService.deletedPayment(paymentId);
    }

    @PutMapping(value = "/update/{paymentId}")
    public void updatePayment(@RequestBody PayMentEntity entity){
        paymentService.updatePayment(entity);
    }
}