package com.example.demo.controller;

import java.util.List;
import com.example.demo.entity.ItemEntity;
import com.example.demo.entity.RestaurantEntity;
import com.example.demo.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/restaurants")
public class RestaurantController {

    private RestaurantService restaurantService;

    @Autowired
    public RestaurantController(RestaurantService restaurantService){
        this.restaurantService = restaurantService;
    }

    @GetMapping("/home")
    public String getHome(){
        return "Welcome to Food Delivery System";
    }

    @GetMapping(value = "/restaurants" , produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RestaurantEntity> getAllRestaurant(){
        return restaurantService.getAllRestaurants();
    }

    @GetMapping(value = "/restaurants/{restId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestaurantEntity getRestaurantById(@PathVariable("restId") long restId){
        return restaurantService.getRestuarantById(restId);
    }

    @GetMapping(value = "/restaurant/name/{key}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RestaurantEntity> getAllRestaurantsByName(@PathVariable("key") String key){
        return restaurantService.getAllRestaurantsByName(key);

    }

    @GetMapping(value = "/restaurant/menu/{menuId}")
    public List<ItemEntity> getMenuItems(@PathVariable("menuId") long menuId){
        return restaurantService.getMenu(menuId);
    }
}