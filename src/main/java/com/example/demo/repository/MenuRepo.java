package com.example.demo.repository;

import java.util.List;

import com.example.demo.entity.MenuEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuRepo extends JpaRepository<MenuEntity,Long>{

    @Query("select distinct p from MenuEntity p where p.menuId = :menuId")
    public List<MenuEntity> getItems(@Param ("menuId") long menuId);
    
}