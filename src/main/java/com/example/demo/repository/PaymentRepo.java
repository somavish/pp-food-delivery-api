package com.example.demo.repository;

import com.example.demo.entity.PayMentEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepo extends JpaRepository<PayMentEntity,Long>{
    
}