package com.example.demo.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "menu_info_details")
public class MenuEntity {
    
    @Id
    @Column(name = "menu_trn_id")
    private long menuTrnId;
    
    @Column(name = "menu_id")
    private long menuId;

    @Column(name = "item_id")
    private long itemId;

    @Column(name ="is_item_available")
    private int isItemAvailable;

    /**
     * @return the menuId
     */
    public long getMenuId() {
        return menuId;
    }

    /**
     * @param menuId the menuId to set
     */
    public void setMenuId(long menuId) {
        this.menuId = menuId;
    }

    /**
     * @return the itemId
     */
    public long getItemId() {
        return itemId;
    }

    /**
     * @param itemId the itemId to set
     */
    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    /**
     * @return the isItemAvailable
     */
    public int getIsItemAvailable() {
        return isItemAvailable;
    }

    /**
     * @param isItemAvailable the isItemAvailable to set
     */
    public void setIsItemAvailable(int isItemAvailable) {
        this.isItemAvailable = isItemAvailable;
    }

    /**
     * @return the menuTrnId
     */
    public long getMenuTrnId() {
        return menuTrnId;
    }

    /**
     * @param menuTrnId the menuTrnId to set
     */
    public void setMenuTrnId(long menuTrnId) {
        this.menuTrnId = menuTrnId;
    }

}