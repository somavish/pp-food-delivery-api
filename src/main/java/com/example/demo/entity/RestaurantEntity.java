package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GenerationType;


@Entity
@Table(name = "restaurant_info_details")
public class RestaurantEntity {

    @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "rest_id")
    private long restId;

	@Column(name ="rest_name")
    private String restName;

	@Column(name = "rest_rating")
    private int restRating;

	@Column(name = "rest_category")
	private int restCateGory;
	
	@Column(name = "menu_id")
	private long menuId;

    public RestaurantEntity(){

    }

    public RestaurantEntity(String name,int rating, int category){
        this.restName = name;
        this.restRating = rating;
        this.restCateGory = category;
    }

	/**
	 * @return the restId
	 */
	public long getRestId() {
		return restId;
	}

	/**
	 * @param restId the restId to set
	 */
	public void setRestId(long restId) {
		this.restId = restId;
	}

	/**
	 * @return the restName
	 */
	public String getRestName() {
		return restName;
	}

	/**
	 * @param restName the restName to set
	 */
	public void setRestName(String restName) {
		this.restName = restName;
	}

	/**
	 * @return the restRating
	 */
	public int getRestRating() {
		return restRating;
	}

	/**
	 * @param restRating the restRating to set
	 */
	public void setRestRating(int restRating) {
		this.restRating = restRating;
	}

	/**
	 * @return the restCateGory
	 */
	public int getRestCateGory() {
		return restCateGory;
	}

	/**
	 * @param restCateGory the restCateGory to set
	 */
	public void setRestCateGory(int restCateGory) {
		this.restCateGory = restCateGory;
	}

	/**
	 * @return the menuId
	 */
	public long getMenuId() {
		return menuId;
	}

	/**
	 * @param menuId the menuId to set
	 */
	public void setMenuId(long menuId) {
		this.menuId = menuId;
	}

    
    
}