package com.example.demo.entity;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "payment_info_details")
public class PayMentEntity {

    enum PaymentStatus{
        SUCCESS,
        PENDING,
        FAILED,
        REFUNDED;

        public static String getPaymentStatus(int val){
            PaymentStatus [] values  = PaymentStatus.values();
            Optional<PaymentStatus> enumValue = Arrays.stream(values).filter(x-> x.ordinal() == val).findFirst();
            return enumValue.isPresent() ? enumValue.get().toString() : "UNKNOWN";
        }
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "payment_id")
    private long paymentId;

    @Column(name = "order_id")
    private long orderId;

    @Column(name = "amount")
    private double amount;

    @Column(name = "payment_status")
	private String paymentStatus;
	
	@Column(name = "input_date_time")
	private LocalDateTime inputDateTime;

	@Column(name = "input_user_id")
	private long inputUserId;

	@Column(name = "last_update_time")
	private LocalDateTime lastUpdateTime;

	@Column(name = "last_update_user_id")
	private long lastUpdateUserId;

	/**
	 * @return the paymentId
	 */
	public long getPaymentId() {
		return paymentId;
	}

	/**
	 * @param paymentId the paymentId to set
	 */
	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}

	/**
	 * @return the orderId
	 */
	public long getOrderId() {
		return orderId;
	}

	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	/**
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}

	/**
	 * @return the paymentStatus
	 */
	public String getPaymentStatus() {
		return paymentStatus;
	}

	/**
	 * @param paymentStatus the paymentStatus to set
	 */
	public void setPaymentStatus(int status) {
		this.paymentStatus = PaymentStatus.getPaymentStatus(status);
	}

	/**
	 * @param paymentStatus the paymentStatus to set
	 */
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	/**
	 * @return the inputDateTime
	 */
	public LocalDateTime getInputDateTime() {
		return inputDateTime;
	}

	/**
	 * @param inputDateTime the inputDateTime to set
	 */
	public void setInputDateTime(LocalDateTime inputDateTime) {
		this.inputDateTime = inputDateTime;
	}

	/**
	 * @return the inputUserId
	 */
	public long getInputUserId() {
		return inputUserId;
	}

	/**
	 * @param inputUserId the inputUserId to set
	 */
	public void setInputUserId(long inputUserId) {
		this.inputUserId = inputUserId;
	}

	/**
	 * @return the lastUpdateTime
	 */
	public LocalDateTime getLastUpdateTime() {
		return lastUpdateTime;
	}

	/**
	 * @param lastUpdateTime the lastUpdateTime to set
	 */
	public void setLastUpdateTime(LocalDateTime lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	/**
	 * @return the lastUpdateUserId
	 */
	public long getLastUpdateUserId() {
		return lastUpdateUserId;
	}

	/**
	 * @param lastUpdateUserId the lastUpdateUserId to set
	 */
	public void setLastUpdateUserId(long lastUpdateUserId) {
		this.lastUpdateUserId = lastUpdateUserId;
	}
}