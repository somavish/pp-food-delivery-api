package com.example.demo.entity;

import java.util.Map;

public class ItemCartEntity {

    private long cartId;

    private Map<Long,Integer> itemsMap;

    private int discount;

    /**
     * @return the cartId
     */
    public long getCartId() {
        return cartId;
    }

    /**
     * @param cartId the cartId to set
     */
    public void setCartId(long cartId) {
        this.cartId = cartId;
    }

    /**
     * @return the itemsMap
     */
    public Map<Long, Integer> getItemsMap() {
        return itemsMap;
    }

    /**
     * @param itemsMap the itemsMap to set
     */
    public void setItemsMap(Map<Long, Integer> itemsMap) {
        this.itemsMap = itemsMap;
    }

    /**
     * @return the discount
     */
    public int getDiscount() {
        return discount;
    }

    /**
     * @param discount the discount to set
     */
    public void setDiscount(int discount) {
        this.discount = discount;
    }

}