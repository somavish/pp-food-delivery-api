package com.example.demo.entity;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "order_info_details")
public class OrderEntity {

    public enum OrderStatus {
        ACCEPTED, FOOD_PREPARING, DECLINED, ON_THE_WAY, DELIVERED;

        public static String getEnumValue(int value) {
            OrderStatus[] valuesArray = OrderStatus.values();
            Optional<OrderStatus> enumName = Arrays.stream(valuesArray).filter(x -> value == x.ordinal()).findFirst();
            return enumName.isPresent() ? enumName.get().toString() : "UNKNOWN";
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "order_id")
    private long orderId;

    @Column(name = "order_status")
    private String orderStatus;

    @Column(name = "payment_id")
    private long paymentId;

    @Column(name = "order_amount")
    private double orderAmount;

    @Column(name = "user_id")
    private long userId;

    @CreationTimestamp
    @Column(name = "last_update_time")
    private LocalDateTime lastUpdateTime;

    @CreationTimestamp
    @Column(name = "input_date_time")
    private LocalDateTime inputDateTime;

    @Column(name = "last_update_user_id")
    private long lastUpdateUserId;

    /**
     * @return the orderId
     */
    public long getOrderId() {
        return orderId;
    }

    /**
     * @param orderId the orderId to set
     */
    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    /**
     * @return the orderStatus
     */
    public String getOrderStatus() {
        return orderStatus;
    }

    /**
     * @param orderStatus the orderStatus to set
     */
    public void setOrderStatus(int orderStatusValue) {
        this.orderStatus = OrderStatus.getEnumValue(orderStatusValue);
    }

    /**
     * @return the paymentId
     */
    public long getPaymentId() {
        return paymentId;
    }

    /**
     * @param paymentId the paymentId to set
     */
    public void setPaymentId(long paymentId) {
        this.paymentId = paymentId;
    }

    /**
     * @return the orderAmount
     */
    public double getOrderAmount() {
        return orderAmount;
    }

    /**
     * @param orderAmount the orderAmount to set
     */
    public void setOrderAmount(double orderAmount) {
        this.orderAmount = orderAmount;
    }

    /**
     * @return the userId
     */
    public long getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    /**
     * @return the lastUpdateTime
     */
    public LocalDateTime getLastUpdateTime() {
        return lastUpdateTime;
    }

    /**
     * @param lastUpdateTime the lastUpdateTime to set
     */
    public void setLastUpdateTime(LocalDateTime lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    /**
     * @return the inputDateTime
     */
    public LocalDateTime getInputDateTime() {
        return inputDateTime;
    }

    /**
     * @param inputDateTime the inputDateTime to set
     */
    public void setInputDateTime(LocalDateTime inputDateTime) {
        this.inputDateTime = inputDateTime;
    }

    /**
     * @return the lastUpdateUserId
     */
    public long getLastUpdateUserId() {
        return lastUpdateUserId;
    }

    /**
     * @param lastUpdateUserId the lastUpdateUserId to set
     */
    public void setLastUpdateUserId(long lastUpdateUserId) {
        this.lastUpdateUserId = lastUpdateUserId;
    }
   
}