package com.example.demo.service;

import java.util.List;
import com.example.demo.entity.ItemEntity;
import org.springframework.stereotype.Service;

@Service
public interface MenuService {
    
    public List<ItemEntity> getItemList(long id);
}