package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.ItemEntity;
import com.example.demo.entity.RestaurantEntity;

import org.springframework.stereotype.Service;

@Service
public interface RestaurantService {

    public List<RestaurantEntity> getAllRestaurants();

    public RestaurantEntity getRestuarantById(long id);

    public List<RestaurantEntity> getAllRestaurantsByName(String name);

    public void insertRestaurant(RestaurantEntity resEntity);

    public void removeRestaurant(long id);

    public List<ItemEntity> getMenu(long id);
    
}