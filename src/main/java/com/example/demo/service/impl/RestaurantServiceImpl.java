package com.example.demo.service.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.example.demo.entity.ItemEntity;
import com.example.demo.entity.RestaurantEntity;
import com.example.demo.repository.RestaurantRepo;
import com.example.demo.service.MenuService;
import com.example.demo.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
public class RestaurantServiceImpl implements RestaurantService {

    @Autowired
    private RestaurantRepo repo;

    @Autowired
    private MenuService menuService;

    @Override
    public List<RestaurantEntity> getAllRestaurants() {
        List<RestaurantEntity> resList = repo.findAll();
        if(!CollectionUtils.isEmpty(resList)){
            return resList.stream().sorted(Comparator.comparingInt(RestaurantEntity::getRestRating).reversed()).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    @Override
    public RestaurantEntity getRestuarantById(long id) {
        Optional<RestaurantEntity> entity = repo.findById(id);
        return entity.isPresent() ? entity.get(): new RestaurantEntity();
    }

    @Override
    public List<RestaurantEntity> getAllRestaurantsByName(String name) {
       List<RestaurantEntity> list = repo.findAll();
       List<RestaurantEntity> resultList = list.stream().filter(x -> x.getRestName().matches(name)).collect(Collectors.toList());
       return !CollectionUtils.isEmpty(resultList) ? resultList : Collections.emptyList();
    }

    @Override
    public void insertRestaurant(RestaurantEntity resEntity){
        repo.save(resEntity);
    }

    @Override
    public void removeRestaurant(long id){
        repo.deleteById(id);
    }

    @Override
    public List<ItemEntity> getMenu(long id){
        return menuService.getItemList(id);
    }
}