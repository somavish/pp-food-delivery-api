package com.example.demo.service.impl;

import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import com.example.demo.entity.ItemCartEntity;
import com.example.demo.entity.ItemEntity;
import com.example.demo.entity.OrderEntity;
import com.example.demo.exceptions.BadRequestException;
import com.example.demo.exceptions.NotFoundExption;
import com.example.demo.repository.ItemRepo;
import com.example.demo.repository.OrderRepo;
import com.example.demo.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepo orderRepo;

    @Autowired
    private ItemRepo itemReo;

    @Override
    public OrderEntity getOrderDetails(long id) {
        Optional<OrderEntity> order;
        if(!orderRepo.existsById(id)){
            throw new NotFoundExption();
        }else{
            order = orderRepo.findById(id);
        }
        return order.isPresent() ? order.get() : new OrderEntity();
    }

    @Override
    public Optional<OrderEntity> placeOrder(OrderEntity entity) {
        if(!orderRepo.existsById(entity.getOrderId())){
            return Optional.ofNullable(orderRepo.save(entity));
        }else{
            throw new BadRequestException();
        }
        
    }

    @Override
    public void deleteOrder(long id) {
        orderRepo.deleteById(id);
    }

    @Override
    public void updateOrder(OrderEntity entity) {
        orderRepo.save(entity);
    }

    @Override
    public Double getItemsCart(ItemCartEntity entity) {
        DoubleSummaryStatistics total;
        Map<Long,Integer> itemsMap = entity.getItemsMap();
        if(!itemsMap.isEmpty()){
            List<ItemEntity> list = itemReo.findAllById(itemsMap.keySet());
            total = list.stream().collect(Collectors.summarizingDouble(c -> c.getItemPrice() *itemsMap.get(c)));
            return total.getSum() - total.getSum() * entity.getDiscount()/100;
        }
        return 0d;

    }
    
}