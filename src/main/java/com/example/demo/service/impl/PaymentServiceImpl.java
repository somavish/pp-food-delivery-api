package com.example.demo.service.impl;

import com.example.demo.entity.PayMentEntity;
import com.example.demo.exceptions.BadRequestException;
import com.example.demo.repository.PaymentRepo;
import com.example.demo.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private PaymentRepo paymentRepo;

    @Override
    public void insertPayment(PayMentEntity entity) {
        if(!paymentRepo.existsById(entity.getPaymentId())){
            paymentRepo.save(entity);
        }else{
            throw new BadRequestException();
        }
    }

    @Override
    public void deletedPayment(long id) {
        paymentRepo.deleteById(id);
    }

    @Override
    public void updatePayment(PayMentEntity entity) {
        paymentRepo.saveAndFlush(entity);
    }
    
}