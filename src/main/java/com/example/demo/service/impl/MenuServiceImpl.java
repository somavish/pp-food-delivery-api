package com.example.demo.service.impl;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import com.example.demo.entity.ItemEntity;
import com.example.demo.entity.MenuEntity;
import com.example.demo.repository.ItemRepo;
import com.example.demo.repository.MenuRepo;
import com.example.demo.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuRepo menuRepo;

    @Autowired
    private ItemRepo itemRepo;

    @Override
    public List<ItemEntity> getItemList(long menuId) {
        List<MenuEntity> menuEntityList = menuRepo.getItems(menuId);
        List<Long> itemIds = menuEntityList.stream().map(MenuEntity::getItemId).collect(Collectors.toList());
        List<ItemEntity> itemList = itemRepo.findAllById(itemIds);      
        return !CollectionUtils.isEmpty(itemList) ? itemList : Collections.emptyList();
    }   
    
}