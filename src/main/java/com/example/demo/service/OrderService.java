package com.example.demo.service;

import java.util.Optional;

import com.example.demo.entity.ItemCartEntity;
import com.example.demo.entity.OrderEntity;
import org.springframework.stereotype.Service;

@Service
public interface OrderService {
    
    public OrderEntity getOrderDetails(long id);

    public Optional<OrderEntity> placeOrder(OrderEntity entity);

    public void deleteOrder(long id);

    public void updateOrder(OrderEntity entity);

    public Double getItemsCart(ItemCartEntity entity);
}