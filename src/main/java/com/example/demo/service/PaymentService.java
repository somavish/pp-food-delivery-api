package com.example.demo.service;

import com.example.demo.entity.PayMentEntity;
import org.springframework.stereotype.Service;

@Service
public interface PaymentService {
    
    public void insertPayment(PayMentEntity entity);

    public void deletedPayment(long id);

    public void updatePayment(PayMentEntity entity);

}