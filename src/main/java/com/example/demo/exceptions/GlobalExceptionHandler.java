package com.example.demo.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javassist.NotFoundException;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler{
    

    @ExceptionHandler(BadRequestException.class)
	public ResponseEntity<Object> badRequestExceptionHandler(BadRequestException ex, WebRequest request) {
        String bodyOfResponse = "Details not found";
        return handleExceptionInternal(ex, bodyOfResponse,  new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
    
    @ExceptionHandler(NotFoundException.class)
	public ResponseEntity<Object> notFoundExceptionHandler(NotFoundException ex, WebRequest request) {
        String bodyOfResponse = "Details not found";
        return handleExceptionInternal(ex, bodyOfResponse,  new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }
    
    @ExceptionHandler(BadGatewayException.class)
	public ResponseEntity<Object> badGatewayExceptionHandler(BadRequestException ex, WebRequest request) {
        String bodyOfResponse = "Bad gateway";
        return handleExceptionInternal(ex, bodyOfResponse,  new HttpHeaders(), HttpStatus.BAD_GATEWAY, request);
    }
}